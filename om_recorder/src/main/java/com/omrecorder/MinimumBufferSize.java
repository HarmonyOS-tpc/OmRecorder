package com.omrecorder;

import ohos.media.audio.AudioCapturer;

/**
 * @author Kailash Dabhi
 * @date 01 July, 2017 12:34 PM
 */
final class MinimumBufferSize {
  private final int minimumBufferSize;

  MinimumBufferSize(AudioRecordConfig audioRecordConfig) {
    this.minimumBufferSize = AudioCapturer.getMinBufferSize(audioRecordConfig.frequency(),
        audioRecordConfig.channelPositionMask().getValue(), audioRecordConfig.audioEncoding().getValue());
  }

  int asInt() {
    return minimumBufferSize;
  }
}
