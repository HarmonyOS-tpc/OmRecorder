package com.omrecorder;

import ohos.media.audio.AudioCapturer;
import ohos.media.audio.AudioCapturerInfo;
import ohos.media.audio.AudioStreamInfo;

/**
 * A {@code Source} represents Audio Record Source.
 *
 * @author Kailash Dabhi
 * @date 01 July, 2017 12:52 AM
 */
interface Source {
    int DEFAULT_SAMPLING_RATE = 44100;

    AudioStreamInfo.ChannelMask DEFAULT_CHANNEL_CONFIG = AudioStreamInfo.ChannelMask.CHANNEL_IN_MONO;

    AudioCapturer audioRecord();

    AudioRecordConfig config();

    int minimumBufferSize();

    class Default implements Source {
        private final AudioCapturer audioRecord;
        private final AudioRecordConfig config;
        private final int minimumBufferSize;

        Default(AudioRecordConfig config) {
            this.config = config;

            this.minimumBufferSize =
                    AudioCapturer.getMinBufferSize(
                            44100,
                            AudioStreamInfo.getChannelCount(AudioStreamInfo.ChannelMask.CHANNEL_IN_MONO),
                            AudioStreamInfo.EncodingFormat.ENCODING_PCM_16BIT.getValue());
            AudioStreamInfo audioStreamInfo = new AudioStreamInfo.Builder()
                    .encodingFormat(AudioStreamInfo.EncodingFormat.ENCODING_PCM_16BIT) // 16-bit PCM
                    .channelMask(DEFAULT_CHANNEL_CONFIG) // 单声道输入
                    .sampleRate(DEFAULT_SAMPLING_RATE) // 44.1kHz
                    .build();

            AudioCapturerInfo.Builder audioCapturerInfo = new AudioCapturerInfo.Builder();
            audioCapturerInfo.audioInputSource(AudioCapturerInfo.AudioInputSource.AUDIO_INPUT_SOURCE_MIC);
            audioCapturerInfo.bufferSizeInBytes(minimumBufferSize * 100);
            audioCapturerInfo.audioStreamInfo(audioStreamInfo);
            this.audioRecord =
                    new AudioCapturer(audioCapturerInfo.build());
        }

        @Override
        public AudioCapturer audioRecord() {
            return audioRecord;
        }


        @Override
        public AudioRecordConfig config() {
            return config;
        }

        @Override
        public int minimumBufferSize() {
            return minimumBufferSize;
        }
    }
}
