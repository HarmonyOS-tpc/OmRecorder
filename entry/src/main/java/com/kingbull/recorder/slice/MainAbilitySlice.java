/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kingbull.recorder.slice;

import com.kingbull.recorder.ResourceTable;
import com.kingbull.recorder.adapter.ListItemProvider;
import com.kingbull.recorder.util.ResUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    private ListItemProvider listItemProvider;

    private final static String DEMO_PCM = "Pcm Recorder";
    private final static String DEMO_WAV = "Wav Recorder";

    String[] demoArray;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        demoArray = new String[] {ResUtil.getPathById(getContext(),ResourceTable.String_pcmrecorder), ResUtil.getPathById(getContext(),ResourceTable.String_wavrecorder) };
        ListContainer recorderList = (ListContainer) findComponentById(ResourceTable.Id_list);

        listItemProvider = new ListItemProvider(this,
                demoArray);
        recorderList.setItemProvider(listItemProvider);
        recorderList.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                switch (demoArray[i]) {
                    case DEMO_PCM:
                        Intent pcmIntent = new Intent();
                        present(new PcmRecorderAbilitySlice(),pcmIntent);
                        break;
                    case DEMO_WAV:
                        Intent wavIntent = new Intent();
                        present(new WavRecorderAbilitySlice(),wavIntent);
                        break;
                }
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
